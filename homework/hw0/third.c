//Peter Weiss
//pweiss9

#include <stdio.h>

// Print "The third prize goes to Ron" followed by a newline and exit
int main(void) {
  printf("The third prize goes to Ron.\n");
  return 0;
}
