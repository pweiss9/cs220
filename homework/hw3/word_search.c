// word_search.c
// Peter Weiss
// pweiss9


#include <stdio.h>
#include <string.h>
#include "search_functions.h"


/*
 * This is the HW3 main function that performs a word search.
 */
int main(int argc, char* argv[]) {

  // Checks that a grid.txt file is entered
  if (argc < 2) {
    printf("Please enter a command line argument.\n");
    return 1;
  }
  
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, argv[1]);
  
  // Checks that populate_grid worked successfully
  if (n == -1) {
    printf("Grid file failed to open.\n");
    return n;
  }
  else if (n == -2) {
    printf("Invalid grid.\n");
    return n;
  }
  
  char input_string[MAX_SIZE + 1]; // Includes NUL terminator
  while (scanf(" %s", input_string) > 0) {
    find_all(grid, n, input_string, stdout);
  }
  return 0;
}
