// test_search_functions.c
// Peter Weiss
// pweiss9


#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "search_functions.h"


/* 
 * Declarations for tester functions whose definitions appear below.
 * (You will need to fill in the function definition details, at the
 * end of this file, and add comments to each one.) 
 * Additionally, for each helper function you elect to add to the 
 * provided search_functions.h, you will need to supply a corresponding
 * tester function in this file.  Add a declaration for it here, its
 * definition below, and a call to it where indicated in main.
 */
void test_file_eq();      // This one is already fully defined below.
void test_populate_grid();
void test_strlwr();
void test_strrev();
void test_find_right();
void test_find_left();
void test_find_down();
void test_find_up();
void test_find_all();


/*
 * Main method which calls all test functions.
 */
int main() {
  printf("Testing file_eq...\n");
  test_file_eq();
  printf("Passed file_eq test.\n\n");

  printf("Running search_functions tests...\n\n");
  test_populate_grid();
  printf("Passed populate_grid test.\n");
  test_strlwr();
  printf("Passed strlwr test.\n");
  test_strrev();
  printf("Passed strrev test.\n");
  test_find_right();
  printf("Passed find_right test.\n");
  test_find_left();
  printf("Passed find_left test.\n");
  test_find_down();
  printf("Passed find_down test.\n");
  test_find_up();
  printf("Passed find_up test.\n");
  test_find_all();
  printf("Passed find_all test.\n\n");

  /* You may add calls to additional test functions here. */

  printf("Passed search_functions tests!!!\n\n");
  return 0;
}


/*
 * Test file_eq on same file, files with same contents, files with
 * different contents and a file that doesn't exist.
 * Relies on files test1.txt, test2.txt, test3.txt being present.
 */
void test_file_eq() {
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  fptr = fopen("test2.txt", "w");
  fprintf(fptr, "this\nis\na different test\n");
  fclose(fptr);

  fptr = fopen("test3.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  assert( file_eq("test1.txt", "test1.txt"));
  assert( file_eq("test2.txt", "test2.txt"));
  assert(!file_eq("test2.txt", "test1.txt"));
  assert(!file_eq("test1.txt", "test2.txt"));
  assert( file_eq("test3.txt", "test3.txt"));
  assert( file_eq("test1.txt", "test3.txt"));
  assert( file_eq("test3.txt", "test1.txt"));
  assert(!file_eq("test2.txt", "test3.txt"));
  assert(!file_eq("test3.txt", "test2.txt"));
  assert(!file_eq("", ""));  // can't open file
}


/*
 * Test populate_grid on two valid grids, grid_test1.txt and grid_test2.txt,
 * and on five invalid grids, grid_test3.txt - grid_test7.txt. One of these
 * files has no contents. Relies on grid_test1.txt - grid_test7.txt being
 * present, however this function also creates those files before it uses
 * them, so they do not need to be in the directory before the function is
 * called.
 */
void test_populate_grid(){
  FILE* fptr = fopen("grid_test1.txt", "w"); // Creates valid grid file
  fprintf(fptr, "aword\nsearc\nhfile\nforte\nsting\n\n");
  fclose(fptr);

  fptr = fopen("grid_test2.txt", "w"); // Creates another valid grid file
  fprintf(fptr, "alongerwor\ndsearchfil\neusedtotes\ntfunctions\n");
  fprintf(fptr, "usedforhom\neworkoneof\nintermedia\n");
  fprintf(fptr, "teprogramm\ningclassth\nissemester\n\n");
  fclose(fptr);

  fptr = fopen("grid_test3.txt", "w"); // Creates grid file
  fprintf(fptr, "abad\nword\nsearc\nfile\n\n"); // Invalid grid file
  fclose(fptr);

  fptr = fopen("grid_test4.txt", "w"); // Creates grid file
  fprintf(fptr, "abad\nword\nsear\nchfi\nleee\n\n"); // Invalid grid file
  fclose(fptr);

  fptr = fopen("grid_test5.txt", "w"); // Creates invalid grid file
  fprintf(fptr, "abadw\nodse\narchf\nileee\n\n");
  fclose(fptr);

  fptr = fopen("grid_test6.txt", "w"); // Createss invalid grid file
  fprintf(fptr, "thiswordsea\nrchfileisba\ndbecauseiti\nstoolongtob\n");
  fprintf(fptr, "eusedcorrec\ntlybecauset\ngridislarge\nrthantenbyt\n");
  fprintf(fptr, "eneventhoug\nhitisasquar\negriditsbad\n\n");
  fclose(fptr);

  fptr = fopen("grid_test7.txt", "w"); // Creates invalid (empty) grid file
  fclose(fptr);

  char grid[MAX_SIZE][MAX_SIZE];
  
  assert(populate_grid(grid, "grid_test1.txt") == 5); // 5 x 5 grid
  // Write expected output to another file to compare
  FILE* file_out1 = fopen("grid_test1_out.txt", "w");
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      fprintf(file_out1, "%c", grid[i][j]);
    }
    fprintf(file_out1, "\n");
  }
  fprintf(file_out1, "\n");
  fclose(file_out1);
  assert(file_eq("grid_test1.txt", "grid_test1_out.txt"));
  
  assert(populate_grid(grid, "grid_test2.txt") == 10); // 10 x 10 grid
  // Write expected output to another file to compare
  FILE* file_out2 = fopen("grid_test2_out.txt", "w");
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      fprintf(file_out2, "%c", grid[i][j]);
    }
    fprintf(file_out2, "\n");
  }
  fprintf(file_out2, "\n");
  fclose(file_out2);
  assert(file_eq("grid_test2.txt", "grid_test2_out.txt"));

  assert(populate_grid(grid, "grid_test3.txt") == -2); // Invalid grid
  assert(populate_grid(grid, "grid_test4.txt") == -2); // Invalid grid
  assert(populate_grid(grid, "grid_test5.txt") == -2); // Invalid grid
  assert(populate_grid(grid, "grid_test6.txt") == -2); // Invalid grid
  assert(populate_grid(grid, "grid_test7.txt") == -2); // Invalid grid
  assert(populate_grid(grid, "") == -1); // Can't open file
}


/*
 * Test strlwr on multiple strings of differing lengths, with letters
 * as well as other charactes in them.
 */
void test_strlwr() {
  char lower1[1];
  char lower2[5];
  char lower3[15];
  
  strlwr("I", lower1, 1);
  assert(strcmp("I", lower1) != 0);
  assert(strcmp("i", lower1) == 0);

  strlwr("HELLO", lower2, 5);
  assert(strcmp("HeLLo", lower2) != 0);
  assert(strcmp("hello", lower2) == 0);

  strlwr("H3LL0", lower2, 5); // Testing with non-alphabetic characters
  assert(strcmp("H3LL0", lower2) != 0);
  assert(strcmp("h3ll0", lower2) == 0);

  strlwr("M1$ch13V0UsNES$", lower3, 15);
  assert(strcmp("M1$ch13V0UsNES$", lower3) != 0);
  assert(strcmp("m1$ch13v0usnes$", lower3) == 0);
}


/*
 * Test strrev on multiple strings of differing lengths.
 */
void test_strrev() {
  char lower1[1];
  char lower2[5];
  char lower3[15];

  strrev("T", lower1, 1);
  assert(strcmp("T", lower1) == 0);

  strrev("HeLlO", lower2, 5);
  assert(strcmp("OlLeH", lower2) == 0);
  strrev("h3l0o", lower2, 5);
  assert(strcmp("o0l3h", lower2) == 0);

  strrev("M1$ch13V0UsNES$", lower3, 15);
  assert(strcmp("$SENsU0V31hc$1M", lower3) == 0);
}


/*
 * Test find_right on a valid grid, with multiple different words to search
 * for. Relies on grid_test2.txt being present and valid, as it is created
 * and tested in test_populate_grid, which is called before test_find_right
 * in main.
 */
void test_find_right(){
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, "grid_test2.txt"); // 10 x 10 valid grid file
  // Two output files compared to check that function works correctly
  FILE* real_output = fopen("real_output.txt", "w");
  FILE* expected_output = fopen("expected_output.txt", "w");

  // Calls find_right, then prints what output should be to expected_output
  assert(find_right(grid, n, "loNGeR", real_output) == 1);
  fprintf(expected_output, "longer 0 1 R\n");
  assert(find_right(grid, n, "me", real_output) == 2);
  fprintf(expected_output, "me 6 5 R\nme 9 4 R\n");
  assert(find_right(grid, n, "O", real_output) == 10);
  fprintf(expected_output, "o 0 2 R\no 0 8 R\no 2 6 R\no 3 7 R\no 4 5 R\n");
  fprintf(expected_output, "o 4 8 R\no 5 2 R\no 5 5 R\no 5 8 R\no 7 4 R\n");
  assert(find_right(grid, n, "die", real_output) == 0);
  fclose(real_output);
  fclose(expected_output);

  assert(file_eq("real_output.txt", "expected_output.txt"));
}


/*
 * Test find_left on a valid grid, with multiple different words to search
 * for. Relies on grid_test2.txt being present and valid, as it is created
 * and tested in test_populate_grid, which is called before test_find_left
 * in main.
 */
void test_find_left(){
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, "grid_test2.txt"); // 10 x 10 valid grid file
  // Two output files compared to check that function works correctly  
  FILE* real_output = fopen("real_output.txt", "w");
  FILE* expected_output = fopen("expected_output.txt", "w");

  // Calls find_left, then prints what output should be to expected_output 
  assert(find_left(grid, n, "Messi", real_output) == 1);
  fprintf(expected_output, "messi 9 4 L\n");
  assert(find_left(grid, n, "SE", real_output) == 2);
  fprintf(expected_output, "se 2 9 L\nse 9 6 L\n");
  assert(find_left(grid, n, "ROWregnola", real_output) == 1);
  fprintf(expected_output, "rowregnola 0 9 L\n");
  assert(find_left(grid, n, "function", real_output) == 0);
  fclose(real_output);
  fclose(expected_output);

  assert(file_eq("real_output.txt", "expected_output.txt"));
}


/*
 * Test find_down on a valid grid, with multiple different wods to search 
 * for. Relies on grid_test1.txt being present and valid, as it is created
 * and tested in test_populate_grid, which is called before test_find_down
 * in main.
 */
void test_find_down(){
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, "grid_test1.txt"); // 5 x 5 valid grid file
  // Two output files compared to checkk that function works correctly
  FILE* real_output = fopen("real_output.txt", "w");
  FILE* expected_output = fopen("expected_output.txt", "w");

  // Calls find_down, then prints what output should be to expected_output
  assert(find_down(grid, n, "dcEEg", real_output) == 1);
  fprintf(expected_output, "dceeg 0 4 D\n");
  assert(find_down(grid, n, "f", real_output) == 2);
  fprintf(expected_output, "f 2 1 D\nf 3 0 D\n");
  assert(find_down(grid, n, "few", real_output) == 0);
  fclose(real_output);
  fclose(expected_output);

  assert(file_eq("real_output.txt", "expected_output.txt"));
}

/*
 * Test find_up on a valid grid, with multiple different words to search
 * for. Relies on grid_test1.txt being present and valid, as it is created
 * and tested in test_populate_grid, which is called before test_find_up in
 * main.
 */
void test_find_up(){
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, "grid_test1.txt"); // 5 x 5 valid grid file
  // Two output files compared to check that function works correctly
  FILE* real_output = fopen("real_output.txt", "w");
  FILE* expected_output = fopen("expected_output.txt", "w");

  // Calls find_up, then prints what output should be to expected_output
  assert(find_up(grid, n, "toFEW", real_output) == 1);
  fprintf(expected_output, "tofew 4 1 U\n");
  assert(find_up(grid, n, "e", real_output) == 3);
  fprintf(expected_output, "e 1 1 U\ne 2 4 U\ne 3 4 U\n");
  assert(find_up(grid, n, "x", real_output) == 0);
  fclose(real_output);
  fclose(expected_output);

  assert(file_eq("real_output.txt", "expected_output.txt"));
}

/*
 * Test find_all on a valid grid, with multiple different words to search 
 * for. Relies on grid_test2.txt being present and valid, as it is created
 * and tested in test_populate_grid, which is called before test_find_all
 * in main.
 */
void test_find_all(){
  char grid[MAX_SIZE][MAX_SIZE];
  int n = populate_grid(grid, "grid_test2.txt"); // 10 x 10 valid grid file
  // Two output files compared to check that function works correctly
  FILE* real_output = fopen("real_output.txt", "w");
  FILE* expected_output = fopen("expected_output.txt", "w");

  // Calls find_all, then printss what output should be to expected_output
  assert(find_all(grid, n, "seMesTer", real_output) == 1);
  fprintf(expected_output, "semester 9 2 R\n");
  assert(find_all(grid, n, "GORPET", real_output) == 1);
  fprintf(expected_output, "gorpet 7 5 L\n");
  assert(find_all(grid, n, "AdEtUEI", real_output) == 1);
  fprintf(expected_output, "adetuei 0 0 D\n");
  assert(find_all(grid, n, "rerdnean", real_output) == 1);
  fprintf(expected_output, "rerdnean 7 3 U\n");
  assert(find_all(grid, n, "z", real_output) == 0);
  fprintf(expected_output, "z - Not Found\n"); // Unique to find_all
  assert(find_all(grid, n, "Peter", real_output) == 0);
  fprintf(expected_output, "peter - Not Found\n"); // Unique to find_all
  assert(find_all(grid, n, "Er", real_output) == 6);
  fprintf(expected_output, "er 0 5 R\ner 6 3 R\ner 9 8 R\n");
  fprintf(expected_output, "er 6 3 D\ner 6 6 D\ner 6 3 U\n");
  fclose(real_output);
  fclose(expected_output);
  
  assert(file_eq("real_output.txt", "expected_output.txt"));
}
