// search_functions.c
// Peter Weiss
// pweiss9


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "search_functions.h"


/* 
 * Given a filename and a MAX_SIZExMAX_SIZE grid to fill, this function 
 * populates the grid and returns n, the actual grid dimension. 
 * If filename_to_read_from can't be opened, this function returns -1.
 * If the file contains an invalid grid, this function returns -2.
 */
int populate_grid(char grid[][MAX_SIZE], char filename_to_read_from[]){
  FILE *input = fopen(filename_to_read_from, "r");
  if (input == NULL) {
    return -1; // Could not open file
  }
  
  int num_col = MAX_SIZE; // Updates each iteration
  // Breaks once rows reaches number of columns (if all rows same length)
  for (int i = 0; i < num_col; i++) {
    // Includes up to MAX_SIZE characters + newline character at the end
    for (int j = 0; j < (MAX_SIZE + 1); j++) {
      char c = '\0';
      int valid = fscanf(input, "%c", &c);
      if (valid != 1) {
	return -2; // Should always get another character in proper file
	           // e.g., file with no data is not properly formatted
      }
      if (j == num_col) {
	if (c != '\n') {
	  return -2; // Too many elements in this row
	}
      }
      if (c == '\n') {
        if (i > 0 && (j != num_col)) {
          return -2; // Too few elements in this row
	}
        else if (i == 0 && j == 0) {
	  return -2; // File starts with newline character
	}
	else {
	  num_col = j; 
	  break;
	}
      }  
      else {
        if (c >= 'A' && c <= 'Z') {
          c = (c - 'A') + 'a';
        }
	else if(!(c >= 'a' && c <= 'z')) {
	  return 2; // Character not a letter
	}
      grid[i][j] = c;
      }
    }
  }
  char c = '\0';
  int eof = fscanf(input, "%c", &c);
  if (!isspace(c) && !(eof == -1)) {
    return -2; // Too many rows in file
  }
  fclose(input);
  return num_col;
}


/*
 * Given two strings of length n, this function takes
 * the characters of upper and stores them into lower, while
 * converting any uppercase letters to lowercase letters. 
 */
void strlwr(char upper[], char lower[], int n) {
  for (int i = 0; i < n; i ++) {
    if (upper[i] >= 'A' && upper[i] <= 'Z') {
      lower[i] = upper[i] - 'A' + 'a';
    }
    else {
      lower[i] = upper[i];
    }
  }
  lower[n] = '\0';
}

/*
 * Given two strings of length n, this function takes the
 * string, forward, reverses it, and stores the result in 
 * the string, reverse.
 */
void strrev(char forward[], char reverse[], int n) {
  for (int i = 0; i < n; i++) {
    reverse[(n - 1) - i] = forward[i];
  }
  reverse[n] = '\0';
}


/* 
 * Given a grid of size n x n, a word to search for, and an output file,
 * this function finds all instances that the word occurs in the 
 * rightward direction, and outputs the location of the start of the
 * word to the output file, write_to. Returns the number of instances
 * of the word in the grid in the rightward direction.
 */
int find_right(char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int word_length = strlen(word);
  char new_word[word_length + 1];
  strlwr(word, new_word, word_length); // Converts word to lowercase

  int count = 0;
  for (int i = 0; i < n; i++) {
    // Only checks up to last column where first letter could be
    for (int j = 0; j < (n - word_length + 1); j++) {
      int match = 1;
      for (int k = 0; k < word_length && match == 1; k++) {
        if (grid[i][j + k] != new_word[k]) {
	  match = 0; // If any letter does not match the word
	}
      }
      if (match == 1) {
        fprintf(write_to, "%s %d %d R\n", new_word, i, j);
        count++;
      }
    }
  }
  return count; // Number of instances in rightward direction
}


/* 
 * Given a grid of size n x n, a word to search for, and an output file,
 * this function finds all instances that the word occurs in the
 * leftward direction, and outputs the location of the start of the
 * word to the output file, write_to. Returns the number of instances
 * of the word in the grid in the leftward direction.
 */
int find_left (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int word_length = strlen(word);
  char new_word[word_length + 1];
  char reverse_word[word_length + 1];
  strlwr(word, new_word, word_length); // Converts word to lowercase
  strrev(new_word, reverse_word, word_length); // Reverses word

  int count = 0;
  for (int i = 0; i < n; i++) {
    // Only checks up to the last column where first letter could be
    for (int j = 0; j < (n - word_length + 1); j++) {
      int match = 1;
      for (int k = 0; k < word_length && match == 1; k++) {
	if (grid[i][j + k] != reverse_word[k]) {
	  match = 0; // If any letter does not match the word
	}
      }
      if (match == 1) {
	// Finds location of last letter of reversed word, which is
	// the same as the location of first letter of word
	fprintf(write_to, "%s %d %d L\n", new_word, i, (j+word_length-1));
	count++;
      }
    }
  }
  return count; // Number of instances in leftward direction
}


/* 
 * Given a grid of size n x n, a word to search for, and an output file,
 * this function finds all instances that the word occurs in the
 * downward direction, and outputs the location of the start of the
 * word to the output file, write_to. Returns the number of instances
 * of the word in the grid in the downward direction. 
 */
int find_down (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int word_length = strlen(word);
  char new_word[word_length + 1];
  strlwr(word, new_word, word_length); // Converts word to lowercase

  int count = 0;
  // Only checks up to the last row where first letter could be
  for (int i = 0; i < (n - word_length + 1); i++) {
    for (int j = 0; j < n; j++) {
      int match = 1;
      for (int k = 0; k < word_length && match == 1; k++) {
	if (grid[i + k][j] != new_word[k]) {
	  match = 0; // If any letter does not match the word
	}
      }
      if (match == 1) {
	fprintf(write_to, "%s %d %d D\n", new_word, i, j);
	count++;
      }
    }
  }
  return count; // Number of instances in downward direction
}


/* 
 * Given a grid of size n x n, a word to search for, and an output file,
 * this function finds all instances that the word occurs in the
 * upward direction, and outputs the location of the start of the
 * word to the output file, write_to. Returns the number of instances
 * of the word in the grid in the upward direction.
 */
int find_up   (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int word_length = strlen(word);
  char new_word[word_length + 1];
  char reverse_word[word_length + 1];
  strlwr(word, new_word, word_length); // Converts word to lowercase
  strrev(new_word, reverse_word, word_length); // Reverses word

  int count = 0;
  // Only checks up to the last row where first letter could be
  for (int i = 0; i < (n - word_length + 1); i++) {
    for (int j = 0; j < n; j++) {
      int match = 1;
      for (int k = 0; k < word_length && match == 1; k++) {
	if (grid[i + k][j] != reverse_word[k]) {
	  match = 0; // If any letter does not match the word
	}
      }
      if (match == 1) {
	// Finds location of last letter of reversed word, which is
	// the same as the location of the first letter of word
	fprintf(write_to, "%s %d %d U\n", new_word, (i+word_length-1), j);
	count++;
      }
    }
  }
  return count; // Number of instances in upward direction
}


/*
 * Given a grid of size n x n, a word to search for, and an output file,
 * this function finds all instances that the word occurs in any direction
 * (other than diagonal), and outputs the location of the start of the 
 * word to the output file, write_to. If the word does not occur in the
 * grid, the function outputs "[word] - Not Found". Returns the number of
 * instances of the word in the grid in any direction.
 */
int find_all  (char grid[][MAX_SIZE], int n, char word[], FILE *write_to) {
  int count = 0;
  count += find_right(grid, n, word, write_to);
  count += find_left(grid, n, word, write_to);
  count += find_down(grid, n, word, write_to);
  count += find_up(grid, n, word, write_to);

  // Prints statement if word does not occur in grid
  if (count == 0) {
    int word_length = strlen(word);
    char new_word[word_length + 1];
    strlwr(word, new_word, word_length); // Converts word to lowercase
    fprintf(write_to, "%s - Not Found\n", new_word);
  }
  return count; // Total number of instances of word in grid
} 


/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match. The definition of this function is provided 
 * for you.
 */
int file_eq(char lhs_name[], char rhs_name[]) {
  FILE* lhs = fopen(lhs_name, "r");
  FILE* rhs = fopen(rhs_name, "r");

  // don't compare if we can't open the files
  if (lhs == NULL || rhs == NULL) return 0;

  int match = 1;
  // read until both of the files are done or there is a mismatch
  while (!feof(lhs) || !feof(rhs)) {
	if (feof(lhs) ||                      // lhs done first
		feof(rhs) ||                  // rhs done first
		(fgetc(lhs) != fgetc(rhs))) { // chars don't match
	  match = 0;
	  break;
	}
  }
  fclose(lhs);
  fclose(rhs);
  return match;
}

