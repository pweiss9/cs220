#ifndef DIGRAPH_H
#define DIGRAPH_H


#include <fstream>
#include <string>
#include <vector>
#include <map>


void sort_digraphs(std::vector<std::string> &digraphs, std::map<std::string, std::vector<std::string>> digraph_map, char c);

void count_sort(std::vector<std::string> &digraphs, std::map<std::string, std::vector<std::string>> digraph_map);

std::vector<std::string> read_file(std::ifstream &file, std::vector<std::string> &digraphs);

std::map<std::string, std::vector<std::string>> create_map(std::vector<std::string> digraphs, std::vector<std::string> text);

bool find_match(std::string digraph, std::string word);

std::string string_tolower(std::string str);

bool string_isalpha(std::string str);

bool isuint(std::string str);

#endif
