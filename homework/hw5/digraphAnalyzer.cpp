#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include "digraph.h"

using std::cin; using std::cout; using std::cerr;
using std::ifstream; using std::stringstream;  using std::endl;
using std::string; using std::vector; using std::map;

/*
 * Main function of digraphAnalyzer.cpp
 * Reads digraphs and text from file entered as command line argument.
 * Prints digraphs found in the text, and the corresponding words that
 * contain these digraphs. Then takes queries from user input about
 * these digraphs, and tells user which digraphs (and in which words)
 * occur a certain number of times, and how many times (and in which
 * words) each digraph appears.
 */
int main(int argc, char *argv[]) {
  if (argc != 3) {
    cerr << "Invalid arguments" << endl;
    exit(1);
  }
  
  ifstream file(argv[1]);
  if (!file.is_open()) {
    cerr << "Invalid file" << endl;
    exit(2);
  }

  if (strlen(argv[2]) != 1) { // Must be a single character
    cerr << "Invalid arguments" << endl;
    exit(1);
  }

  vector<string> digraphs;

  // Reads digraphs and words (into vectors of strings) from input file
  vector<string> text = read_file(file, digraphs);

  // Creates map from digraph to words that contain that digraph
  map<string, vector<string>> digraph_map = create_map(digraphs, text);

  sort_digraphs(digraphs, digraph_map, *argv[2]);
  
  for (vector<string>::const_iterator digraph_it = digraphs.cbegin();
       digraph_it != digraphs.cend();
       digraph_it++) {
    cout << *digraph_it << ": ["; // Prints digraph
    vector<string> words = digraph_map[*digraph_it];
    for (vector<string>::const_iterator word_it = words.cbegin();
	 word_it != words.cend();
	 word_it++) {
      cout << *word_it; // Prints each word that contains digraph
      if (word_it != (words.cend() - 1)) {
	cout << ", "; // Formatting
      }
    }
    cout << "]" << endl; // Formatting
  }

  // From now on, sort using ASCII order
  sort_digraphs(digraphs, digraph_map, 'a');
  string query;
  size_t query_num;
  string query_string;

  do {
    cout << "q?>";
    cin >> query;
    if (string_isalpha(query)) { // Only words can contain digraphs
      query_string = string_tolower(query);
      if (query_string != "quit") { // Keyword used to exit program

	// Checks if the digraph is in the list of input digraphs
	if ((std::find(digraphs.begin(), digraphs.end(), query_string)) != digraphs.end()) {
	  vector<string> words = digraph_map[query_string];
	  cout << words.size() << endl; // How many words contain digraph
	  for (vector<string>::const_iterator it = words.cbegin();
	       it != words.cend();
	       it++) {
            cout << *it << endl; // Prints words containing digraph
          }
	}
	else {
	  cout << "No such digraph" << endl;
	}
      }
    }
    else if (isuint(query)) { // Checks if string is unsigned integer
      stringstream(query) >> query_num; // Converts string to size_t
      bool none = true;
      for (vector<string>::const_iterator digraph_it = digraphs.cbegin();
	   digraph_it < digraphs.cend();
	   digraph_it++) {
        if ((digraph_map[*digraph_it]).size() == query_num) {
	  cout << *digraph_it << endl; // Prints digraph
	  vector<string> words = digraph_map[*digraph_it];
	  for (vector<string>::const_iterator word_it = words.cbegin();
	       word_it < words.cend();
	       word_it++) {
            cout << *word_it << endl; // Prints words containing digraph
	  }
	  none = false; // If any digraphs occur that many times
	}
      }
      if (none) {
        cout << "None" << endl;
      }
    }
    else { // If input is not a valid digraph or a valid number
      cerr << "Invalid arguments" << endl;
      exit(1);
    }
  }
  while (query_string != "quit");
    
  return 0;
}
