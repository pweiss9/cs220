#include "digraph.h"
#include <iostream>
#include <cctype>
#include <algorithm>

using std::cerr; using std::endl; using std::ifstream;
using std::vector; using std::map; using std::string;


/*
 * Sorts a vector of strings containing the digraphs,
 * according to the character c. 'a' means ASCII order
 * 'r' means reverse ASCII order, 'c' means "count" order,
 * i.e. largest to smallest number of words containing that digraph,
 * with ties broken by ASCII order. Note thatdigraph_maps is needed
 * to compute how many words contain each digraph.
 * If c is anything else, exits with error code 1.
 */
void sort_digraphs(vector<string> &digraphs, map<string, vector<string>> digraph_map, char c) {
  switch (c) {
    case 'a' :
      sort(digraphs.begin(), digraphs.end());
      break;
    case 'r' :
      sort(digraphs.begin(), digraphs.end(), std::greater<string>());
      break;
    case 'c' :
      // Recursive call used to break ties using ASCII order
      sort_digraphs(digraphs, digraph_map, 'a');
      count_sort(digraphs, digraph_map);
      break;
    default :
      cerr << "Invalid arguments" << endl;
      exit(1);
  }
}


/*
 * Sorts a vector of strings containing the digraphs according to
 * the size of the vector of words from the input file that is the
 * value of the map, digraph_map, with the corresponding key being
 * that digraph (i.e. according to how many words contain that digraph).
 * Digraphs sorted from largest number of associated words to smallest
 * number of associated words. Assumes the digraphs vector is already
 * sorted in ASCII order.
 */
void count_sort(vector<string> &digraphs, map<string, vector<string>> digraph_map) {

  /* Keys are number of occurences of a digraph. Values are vectors of
     all the digraphs that occur that many times. */
  map<size_t, vector<string>> count_map;
  for (vector<string>::const_iterator digraph_it1 = digraphs.cbegin();
       digraph_it1 != digraphs.cend();
       digraph_it1++) {
    (count_map[(digraph_map[*digraph_it1]).size()]).push_back(*digraph_it1);
  }
  
  // Allows us to entirely reorder the digraphs vector
  digraphs.erase(digraphs.begin(), digraphs.end());

  // Iterates over number of occurences of each digraph
  for (map<size_t, vector<string>>::const_reverse_iterator map_it = count_map.crbegin();
       map_it != count_map.crend();
       map_it++) {
    
    /* Vector of all the digraphs that occur
       as many times as the key of the map */
    vector<string> digraphs_count = map_it->second; 

    // Iterates over digraphs that occur this many times
    for (vector<string>::const_iterator digraph_it2 = digraphs_count.cbegin();
	 digraph_it2 != digraphs_count.cend();
	 digraph_it2++) {

      /* Adds digraphs to digraphs vector in appropriate order.
         Note that since digraphs vector was already in ASCII order,
         for digraphs that occur the same number of times,
         ASCII order is maintained */
      digraphs.push_back(*digraph_it2);
    }
  }
}


/*
 * Reads from the input file stream, file, storing the digraphs into
 * the vector of strings, digraphs, and storing the remaining text in
 * the file into another vector of strings, deleting any punctuation.
 * Returns the vector of strings containing the words in the file.
 */
vector<string> read_file(ifstream &file, vector<string> &digraphs) {
  int num_digraphs;
  string digraph;
  file >> num_digraphs;
  for (int i = 0; i < num_digraphs; i++) {
    file >> digraph;
    digraph = string_tolower(digraph); // converts to lowercase
    digraphs.push_back(digraph);
  }
  
  vector<string> text;
  string word;
  while (file >> word) {
    char c;
    for (size_t i = 0; i < word.length(); i++) {
      c = word[i];
      if (c == ',' || c == '.' || c == '!' || c == '?') {
	word.erase(word.begin() + i);
	i--; // Makes sure we don't miss any characters
      }
    }
    word = string_tolower(word); // converts to lowercase
    text.push_back(word);
  }

  return text;
}


/*
 * Creates a map, using the digraphs (strings) as the keys, and the 
 * words in the text vector that contain each digraph (vectors of strings)
 * as the values. Returns this map of a string to a vector of strings.
 */
map<string, vector<string>> create_map(vector<string> digraphs, vector<string> text) {
  map<string, vector<string>> digraph_map;
  for (vector<string>::const_iterator digraph_it = digraphs.cbegin();
       digraph_it != digraphs.cend();
       digraph_it++) {
    for (vector<string>::const_iterator text_it = text.cbegin();
	 text_it != text.cend();
	 text_it++) {
      if (find_match(*digraph_it, *text_it)) { // if word contains digraph
        (digraph_map[*digraph_it]).push_back(*text_it);
      }
    }
  }
  return digraph_map;
}


/*
 * Returns true if the string, word, contains the string, digraph.
 */
bool find_match(string digraph, string word) {
  if (digraph.length() > word.length()) {
    return false;
  }
  // Checks all possible substrings
  for (size_t i = 0; i < (word.length() - digraph.length() + 1); i++) {
    if (word.substr(i, digraph.length()) == digraph) {
      return true;
    }
  }
  return false;
}


/*
 * Converts every uppercase character in a string to lowercase.
 * Returns this string, containing all lowercase letters.
 */
string string_tolower(string str) {
  for (size_t i = 0; i < str.length(); i++) {
    str[i] = tolower(str[i]);
  }
  return str;
}


/*
 * Returns true if every character in string, str, is a letter.
 */
bool string_isalpha(string str) {
  for (size_t i = 0; i < str.length(); i++) {
    if (!(isalpha(str[i]))) {
      return false;
    }
  }
  return true;
}


/*
 * Returns true if the string, str, contains an unsigned integer.
 */
bool isuint(string str) {
  if (!(isdigit(str[0]) || (str[0] == '+'))) { // Allows for leading + sign
    return false;
  }
  for (size_t i = 1; i < str.length(); i++) {
    if (!(isdigit(str[i]))) {
      return false;
    }
  }
  return true;
}
