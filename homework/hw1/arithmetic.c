//Peter Weiss
//pweiss9

#include <stdio.h>

int main(void) {
  printf("Please enter an arithmetic expression using * and / only:\n");
  
  float answer = 0;
  float new_num = 0;
  char operator = '\0';
  int malformed = 0;
  int div_by_zero = 0;
  
  if (scanf("%f", &answer) != 1) {
    malformed = 1;
  }
  while ((scanf(" %c",&operator) == 1)&&(malformed == 0)&&(div_by_zero == 0)) {
    if (scanf("%f", &new_num) != 1) {
      malformed = 1;
    }
    else {
      switch (operator) {
      case '*' :
	answer*= new_num;
	break;
      case '/' :
	if (new_num < 0.00001 && new_num > -0.00001) {
	  div_by_zero = 1;
	}
	else {
	  answer/= new_num;
	}
	break;
      default :
	malformed = 1;
	break;
      }
    }
  }
  if (div_by_zero != 0) {
    printf("division by zero\n");
    return 2;
  }
  else if (malformed != 0) {
    printf("malformed expression\n");
    return 1;
  }
  else {
    printf("%f\n", answer);
    return 0;
  }
}    
