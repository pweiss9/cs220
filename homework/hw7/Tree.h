#ifndef _TREE_H
#define _TREE_H

#include <cstdlib>
#include <string>

// tree of any data type with <, ==, and = operators defined on it
// Can be used to implement a trie

template <class T>
class Tree {
  friend class TTreeTest;

  T data;       // the value sstored in the tree node

  Tree * kids;  // children - pointer to the first child of list

  Tree * sibs;  // siblings - pointer to res of children list

  Tree * prev;  // pointer to parent if first child, or sibling otherwise

 public:
  
  template<typename U>
  friend std::ostream& operator<<(std::ostream& os, Tree<U>& rt);

  Tree(T val); // Constructor

  ~Tree(); // Destructor (clears siblings to right and children of this node)

  Tree& operator^(Tree& rt); //^ operator (same function ass addChild)

  bool operator==(const Tree& root); // return true if Trees match exactly

  bool addChild(T val); // returns true if children unique, false otherwise

  bool addChild(Tree<T> *root); // returns false if any failure upon adding
  
  std::string toString(); // all data, separated by newlines

 private:

  bool addSibling(T val); // returns false if any failure upon adding

  bool addSibling(Tree *root); // returns false if any failure upon adding
};

#include "Tree.inc" // Implementation of all functions above

#endif 
