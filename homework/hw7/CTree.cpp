#include <iostream>
#include <string>
#include "CTree.h"

using std::ostream;
using std::string;

/*
 * Overloaded insertion operator, printing the
 * CTree, rt, to output. Returns the output stream.
 */
ostream& operator<<(ostream& os, CTree& rt) {
  os << rt.toString();
  return os;
}

/*
 * Constructor for CTree, taking the character stored in the
 * root node as a parameter, and initializing the links to
 * children, siblings, and previous (parent or sibling) to NULL pointer.
 */
CTree::CTree(char ch) : data(ch), kids(NULL), sibs(NULL), prev(NULL) {}

/*
 * Destructor for CTree, deleting any dynamically allocated children
 * or siblings (recursively if those nodes have children or siblings).
 */
CTree::~CTree() {
  delete kids;
  delete sibs;
}

/*
 * Overloaded ^ (carat) operator, adding CTree, rt (which must be a
 * root node), as a child, returning the new root of the tree, which
 * now has additional children (if the child was added successfully).
 */
CTree& CTree::operator^(CTree& rt) {
  addChild(&rt);
  return *this;
}

/*
 * Overloaded == (equality) operator, returning true if both
 * CTrees have the exact same nodes (same children, same siblings,
 * and same data in all nodes).
 */
bool CTree::operator==(const CTree &root) {
  if ((data == root.data)) {
    return false; // Data doesn't match
  }
  if ((kids && !(kids)) || (!(kids) && root.kids)) {
    return false; // Trees have a different number of children
  }
  if ((sibs && !(root.sibs)) || (!(sibs) && root.sibs)) {
    return false; // Trees have a different number of siblings
  }
  if (!(kids) && !(sibs)) {
    return true; // Trees both have no siblings or children and same data
  }
  if (!(kids)) { // Trees both have siblings but no children
    return (*(sibs) == *(root.sibs)); // Recursive call
  }
  if (!(this->sibs)) { // Trees both have children but no siblings
    return (*(kids) == *(root.kids)); // Recursive call
  }
  
  // Trees both have both children and siblings
  // Recursive call
  return ((*(kids) == *(root.kids)) && (*(sibs) == *(root.sibs)));
}

/*
 * Overloaded addChild method that takes a character to add as a
 * new child node as a parameter, and returns true if it was added
 * successfully. Will return false if that character is already data
 * that is stored in a child. 
 */
bool CTree::addChild(char ch) {
  CTree *child = new CTree(ch);
  if (!(addChild(child))) { // Calls other method
    delete child; // Node couldn't be added, so we need to delete it
    return false;
  }
  return true;
}

/*
 * Overloaded addChild method that takes an existing root node to add as
 * a new child node as a parameter, and returns true if it was added
 * successfully. Will return false if the data in this node is already
 * data that is stored in a child, or if this node is not a root node.
 */
bool CTree::addChild(CTree *root) {
  if (root->prev || root->sibs) {
    return false; // Node is not a root node
  }
  if (!(kids)) { // Tree has no children, so no data to check
    kids = root;
    root->prev = this;
    return true;
  }
  if (root->data < kids->data) { // Node is the new first child
    root->prev = this;
    root->sibs = kids;
    kids->prev = root;
    kids = root;
    return true;
  }
  if (root->data == kids->data) {
    return false; // Node has same data as first child
  }
  return kids->addSibling(root); // Private helper function
}

/*
 * toString method that creates a string of the data in all nodes that
 * are children or siblings of the root node (separated by newlines)
 * using a depth-first, pre-order traversal. Returns this string.
 */
string CTree::toString() {
  string str;
  str.push_back(data);
  str.push_back('\n');
  if (kids) {
    str.append(kids->toString()); // Recursive call to children (first)
  }
  if (sibs) {
    str.append(sibs->toString()); // Recursive call to siblings (second)
  }
  return str;
}

/*
 * Private, overloaded helper function, addSibling, that adds a sibling,
 * with data ch, to the current node. Returns true if sibling added
 * successfully. Will return false if there is already a sibling
 * with data ch.
 */
bool CTree::addSibling(char ch) {
  CTree *sibling = new CTree(ch);
  if (!(addSibling(sibling))) { // Calls other method
    delete sibling; // Node couldn't be added, so we need to delete it
    return false;
  }
  return true;
}

/* 
 * Private, overloaded helper function, addSibling, takes a pre-existing,
 * root node as a parameter to add as a new sibling. Returns true if
 * sibling added sucessfully. Will return false if there is already a
 * sibling with the same data or if this node is not a root node.
 */
bool CTree::addSibling(CTree *root) {
  if (!(prev)) {
    return false; // Cannot be a sibling if it does not have a parent
  }
  if (root->prev || root->sibs) {
    return false; // Node is not a root node 
  }
  CTree *sibling = this;
  while (sibling->sibs && (sibling->sibs->data < root->data)) {
    // Extracts sibling right before where this node needs to be added
    sibling = sibling->sibs;
  }
  if (!(sibling->sibs)) { // Node is the new last sibling
    sibling->sibs = root;
    root->prev = sibling;
    return true;
  }
  if (sibling->sibs->data == root->data) {
    return false; // Node has same data as a sibling
  }

  // Adds node between two siblings
  root->sibs = sibling->sibs;
  root->prev = sibling;
  sibling->sibs->prev = root;
  sibling->sibs = root;
  return true;
}
