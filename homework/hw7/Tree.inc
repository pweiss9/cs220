#include <iostream>
#include <string>
#include <sstream>

/*
 * Overloaded insertion operator, printing the
 * Tree, rt, to output. Returns the output stream.
 */
template<typename U>
std::ostream& operator<<(std::ostream& os, Tree<U>& rt) {
  os << rt.toString();
  return os;
}

/*
 * Constructor for Tree, taking the data stored in the root
 * node as a parameter, and initializing the links to children,
 * siblings, and previous (parent or sibling) to NULL pointer.
 */
template<typename T>
Tree<T>::Tree(T val) : data(val), kids(NULL), sibs(NULL), prev(NULL) {}

/*
 * Destructor for Tree, deleting any dynamically allocated children
 * or siblings (recursively if those nodes have children or siblings).
 */
template<typename T>
Tree<T>::~Tree() {
  delete kids;
  delete sibs;
}

/*
 * Overloaded ^ (carat) operator, adding Tree, rt (which must be a
 * root node), as a child, returning the new root of the tree, which
 * now has additional children (if the child was added successfully).
 */
template<typename T>
Tree<T>& Tree<T>::operator^(Tree<T>& rt) {
  addChild(&rt);
  return *this;
}

/*
 * Overloaded == (equality) operator, returning true if both
 * Trees have the exact same nodes (same children, same siblings,
 * and same data in all nodes).
 */
template<typename T>
bool Tree<T>::operator==(const Tree<T>& root) {
  if (!(data == root.data)) {
    return false; // Data doesn't match
  }
  if ((kids && !(root.kids)) || (!(kids) && root.kids)) {
    return false; // Trees have a different number of children
  }
  if ((sibs && !(root.sibs)) || (!(sibs) && root.sibs)) {
    return false; // Trees have a different number of siblings
  }
  if (!(kids) && !(sibs)) {
    return true; // Trees both have no siblings or children and same data
  }
  if (!(kids)) { // Trees both have siblings but no children
    return (*(sibs) == *(root.sibs)); // Recursive call
  }
  if (!(sibs)) { // Trees both have children but no siblings
    return (*(kids) == *(root.kids)); // Recursive call
  }

  // Trees both have both children and siblings
  // Recursive call
  return ((*(kids) == *(root.kids)) && (*(sibs) == *(root.sibs)));
}

/*
 * Overloaded addChild method that takes data to add as a new child as
 * a parameter, and returns true if it was added successfully. Will return
 * false if that data is already data that is stored in a child.
 */
template<typename T>
bool Tree<T>::addChild(T val) {
  Tree<T> *child = new Tree<T>(val);
  if (!(addChild(child))) { // Calls other method
    delete child; // Node couldn't be added, so we need to delete it
    return false;
  }
  return true;
}

/*
 * Overloaded addChild method that takes an existing root node to add
 * as a new child node as a parameter, and returns true if it was added
 * successfully. Will return false if the data in this node is already
 * data that is stored in a child, or if this node is not a root node.
 */
template<typename T>
bool Tree<T>::addChild(Tree<T> *root) {
  if (root->prev || root->sibs) {
    return false; // Node is not a root node
  }
  if (!(kids)) { // Tree has no chidren, so no data to check
    kids = root;
    root->prev = this;
    return true;
  }
  if (root->data < kids->data) { // Node is the new first child
    root->prev = this;
    root->sibs = kids;
    kids->prev = root;
    kids = root;
    return true;
  }
  if (root->data == kids->data) {
    return false; // Node has same data as first child
  }
  return kids->addSibling(root); // Private helper function
}

/*
 * toString method that creates a string of the data in all nodes that
 * are children or siblings of the root node (separated by newlines)
 * using a depth-first, pre-order traversal. Returns this string.
 */
template<typename T>
std::string Tree<T>::toString() {
  std::stringstream tree_stream;
  tree_stream << data;
  tree_stream << '\n';
  if (kids) {
    tree_stream << kids->toString(); // Recursive call to children (first)
  }
  if (sibs) {
    tree_stream << sibs->toString(); // Recursive call to siblings (second)
  }
  return tree_stream.str();
}

/*
 * Private, overloaded helper function, addSibling, that adds a sibling,
 * with data val, to the current node. Returns true if sibling added
 * successfully. Will return false if there is already a sibling
 * with data val.
 */
template<typename T>
bool Tree<T>::addSibling(T val) {
  Tree<T> *sibling = new Tree<T>(val);
  if (!(addSibling(sibling))) { // Calls other method
    delete sibling; // Node couldn't be added, so we need to delete it
    return false;
  }
  return true;
}

/*
 * Private, overloaded helper function, addSibling, takes a pre-existing,
 * root node as a parameter to add as a new sibling. Returns true if
 * sibling added successfully. Will return false if there is already a
 * sibling with the same data or if this node is not a root node.
 */
template<typename T>
bool Tree<T>::addSibling(Tree<T> *root) {
  if (!(prev)) {
    return false; // Cannot be a sibling if it does not have a parent
  }
  if (root->prev || root->sibs) {
    return false; // Node is not a root node
  }
  Tree<T> *sibling = this;
  while (sibling->sibs && (sibling->sibs->data < root->data)) {
    // Extracts sibling right before wherer this node needs to be added
    sibling = sibling->sibs;
  }
  if (!(sibling->sibs)) { // Node is the new last sibling
    sibling->sibs = root;
    root->prev = sibling;
    return true;
  }
  if (sibling->sibs->data == root->data) {
    return false; // Node has same data as a sibling
  }

  // Adds a node between two siblings
  root->sibs = sibling->sibs;
  root->prev = sibling;
  sibling->sibs->prev = root;
  sibling->sibs = root;
  return true;
}
