#include <stdio.h>
#include <string.h>


/*
    Returns in the third argument the concatenation of the first
    argument and the second argument, provided that there is
    sufficient space in third argument, as specified by the fourth.
    e.g.
        concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
        concat("alpha", "gamma", result, 10) puts nothing into result and returns 1
*/
int concat(const char word1[], const char word2[], char result[], int result_capacity){

   //TODO: replace the following stub with a correct function body so that 
   //      this function behaves as indicated in the comment above
   //
   //NOTE: you may not use the strcat or strcpy library functions in your solution!
  if ((int)((strlen(word1) + strlen(word2))) >= result_capacity)
    return -1;
  else {
    long unsigned int i = 0;
    long unsigned int j = 0;
    for (i = 0; i < strlen(word1); i++) {
      result[i] = word1[i];
    }
    for (j = 0; j < strlen(word2); j++) {
      result[i + j] = word2[j];
    }
    result[i + j] = '\0';
    return 0;
  }
}
