
#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

// put char into a newly created node
Node * create_node(char ch) {
  Node * node = (Node *) malloc(sizeof(Node));
  assert(node); //confirm malloc didn't fail

  node->data = ch;
  node->next = NULL;
  return node;
}

// output the list iteratively
void print(const Node * cur) {
  while (cur != NULL) {
    printf("%c ", cur->data);
    cur = cur->next;  // advance to next node
  }
}


long length(const Node * cur) {
  long i = 0;
  while (cur != NULL) {
    i++;
    cur = cur->next;
  }
  return i;
}


void add_after(Node * node, char val) {
  Node *new_node = create_node(val);
  Node *temp = node->next;
  node->next = new_node;
  new_node->next = temp;
}


void reverse_print(const Node * head) {
  const Node *cur = head;
  long i = length(cur);
  for (i; i > 0; i--) {
    cur = head;
    for (int j = 0; j < i; j++) {
      if (j == (i - 1)) {
	printf("%c ", cur->data);
      }
      cur = cur->next;
    }
  }
}
